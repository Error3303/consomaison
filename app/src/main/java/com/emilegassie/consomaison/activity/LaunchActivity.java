package com.emilegassie.consomaison.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;

import androidx.appcompat.app.AppCompatActivity;

import com.emilegassie.consomaison.R;

public class LaunchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        initialize();
    }

    private void initialize(){
        CountDownTimer timer = new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                //do nothing
            }

            @Override
            public void onFinish() {
                launchMainActivity();
            }
        };
        timer.start();
    }

    private void launchMainActivity(){
        Intent launchMain = new Intent(this, MainActivity.class);
        startActivity(launchMain);
        this.finish();
    }
}
