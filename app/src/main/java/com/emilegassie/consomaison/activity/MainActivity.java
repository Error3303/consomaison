package com.emilegassie.consomaison.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.emilegassie.consomaison.R;
import com.emilegassie.consomaison.adapter.ViewPagerAdapter;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    TabLayout tabLayoutMenu;

    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tabLayoutMenu = findViewById(R.id.tab_layout_menu);
        viewPager = findViewById(R.id.view_pager);
        // Get the ViewPager and set it's PagerAdapter so that it can display items
//        ElectricityFragment fragment = (ElectricityFragment) getSupportFragmentManager().findFragmentById(R.id.main_fragment);
//        fragment.getActivity();

        initialize();
    }

    private void test(){
        System.out.println("blabla");
    }

    private void initialize(){
        tabLayoutMenu.addTab(tabLayoutMenu.newTab().setText(getString(R.string.title_elec)));
        tabLayoutMenu.addTab(tabLayoutMenu.newTab().setText(getString(R.string.title_eau)));

        final ViewPagerAdapter adapter = new ViewPagerAdapter(this,getSupportFragmentManager(),
                tabLayoutMenu.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayoutMenu));
        tabLayoutMenu.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }
}
