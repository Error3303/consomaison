package com.emilegassie.consomaison.adapter;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.emilegassie.consomaison.fragment.ElectricityFragment;
import com.emilegassie.consomaison.fragment.WaterFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    Context context;
    int totalTabs;
    public ViewPagerAdapter(Context c, FragmentManager fm, int totalTabs) {
        super(fm);
        context = c;
        this.totalTabs = totalTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                ElectricityFragment electricityFragment = new ElectricityFragment();
                return electricityFragment;
            case 1:
                WaterFragment waterFragment = new WaterFragment();
                return waterFragment;
            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return totalTabs;
    }
}
