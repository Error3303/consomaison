package com.emilegassie.consomaison.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.emilegassie.consomaison.R;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ElectricityFragment extends Fragment {

    @BindView(R.id.edit_electricity_statement)
    TextView editReleve;

    @BindView(R.id.btn_add_electricity_statement)
    ImageButton btnAjouterReleve;

    @BindView(R.id.btn_electricity_stats)
    Button btnStats;

    @BindView(R.id.btn_electricity_settings)
    Button btnParametres;

    @BindView(R.id.lbl_value_electricity_month)
    TextView tvMonth;

    @BindView(R.id.lbl_value_average_consumption)
    TextView tvAverageConsumption;

    @BindView(R.id.lbl_value_electricity_approximative_price)
    TextView tvApproximativePrice;

    ElectricityFragmentViewModel viewModel;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_elec, container, false);
        ButterKnife.bind(this, view);

        initializeComponent();

        return view;
    }

    private void initializeComponent() {
        viewModel = ViewModelProviders.of(this).get(ElectricityFragmentViewModel.class);
        btnAjouterReleve.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    updateApproximativePrice();
                }
                return false;
            }
        });
    }

    private void updateApproximativePrice() {
        tvApproximativePrice.setText(String.format(Locale.getDefault(), "%.2f €", viewModel.calculateApproximativePrice()));
    }
}
